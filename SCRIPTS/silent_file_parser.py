import sys


def parse_silent(file, output):
    with open(file, 'r') as infile, open(output, 'w') as outfile:
        new_line = []
        ss_list = []
        structure = []
        for line in infile:
            if line.find('S_000') != -1:
                stripped_line = line.strip()
                line_list = stripped_line.split()
                if line.startswith('SCORE:') and line_list[18] != structure:
                    ss = ''.join(ss_list)
                    new_line.append(ss)
                    outfile.write(' '.join(new_line))
                    outfile.write('\n')
                    structure = line_list[18]
                    new_line = []
                    ss_list = []
                    for i in 1, 9, 13, 18:
                        new_line.append(line_list[i])
                if len(line) == 73:
                    stripped_line = line.strip()
                    list_line = stripped_line.split()
                    ss_list.append(list_line[1])
        ss = ''.join(ss_list)
        new_line.append(ss)
        outfile.write(' '.join(new_line))
        outfile.write('\n')


if __name__ == '__main__':
    silent_file = sys.argv[1]
    test_out = silent_file+'_parsed'
    parse_silent(silent_file, test_out)
